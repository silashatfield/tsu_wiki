FROM mediawiki:1.29

COPY ./LocalSettings.php /var/www/html/LocalSettings.php
COPY ./extensions /var/www/html/extensions
COPY ./edit.php /var/www/html/edit.php
COPY ./changesboard.php /var/www/html/changesboard.php
COPY ./dbsettings.php /var/www/html/dbsettings.php
COPY ./runproc.php /var/www/html/runproc.php