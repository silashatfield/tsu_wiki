<?php
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);
	mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);

	require_once("dbsettings.php");
	$connection = mysqli_connect($wgDBserver, $wgDBuser, $wgDBpassword, $wgDBname, "3306");
	$rows = array();

	$type = $_GET["type"]; //nextid, changelist

	if($type == "nextid"){
		$sql = "
			select convert(replace(page_title,'Change_',''),UNSIGNED INTEGER)+1 as ID
			from tw_page
					inner join tw_revision on page_Latest = rev_id
					inner join tw_text on rev_text_id = old_id
			where page_title like 'Change_%'
				and old_text like '%Category:Changes_Board%'
			order by ID DESC
			limit 1;
		";
		if ($result = $connection->query($sql)) {
			while($row = $result->fetch_assoc()) {
				$rows[] = $row;
			}
			$result->free_result();			
		}
	} elseif($type == "changelist"){
		$limit = 50;
		if(!empty($_GET["limit"])){
			$limit = $_GET["limit"];
		}
		$sql = "
			select convert(replace(page_title,'Change_',''),UNSIGNED INTEGER) as ID
				,old_text as old_text
				,regexp_replace(replace(replace(old_text,'\n',''),'\r',''),'.*?Originally Posted : (.*?)(Posted|<br>).*','$1') as OriginallyPostedDate 
				,regexp_replace(replace(replace(old_text,'\n',''),'\r',''),'.*?Posted by : (.*?)(Title|<br>).*','$1') as Wizard
				,regexp_replace(replace(replace(old_text,'\n',''),'\r',''),'.*?Title : (.*?)(<br>).*','$1') as Title 
				,regexp_replace(replace(replace(replace(old_text,'\n',''),'\r',''), '[[Category:Changes_Board]]', ''),'.*?Title : (.*?)(<br>)(.*)','$3') as Article 
			from tw_page
					inner join tw_revision on page_Latest = rev_id
					inner join tw_text on rev_text_id = old_id
			where 1=1
				#MUST INCLUDE THESE FOR CHANGES BOARD
				and page_title like 'Change_%'
				and old_text like '%Category:Changes_Board%'	
			
				#CASE ID = [num]
				#and convert(replace(page_title,'Change_',''),UNSIGNED INTEGER) between ? and ?
			
				#ORDER BY
			order by ID DESC
			
				#LIMIT
			limit ?;	
		";
		if ($stmt = $connection->prepare($sql)) {
			// Pass the parameters
			$stmt->bind_param("i", $limit); 
			// Execute the query
			
			if ($result = $stmt->execute()) {
				$stmt->bind_result($id, $text,$posteddate,$wizard,$title,$article);

				while ($stmt->fetch()) {
					$row = (object) ["id" => $id
						, "text" => $text
						, "posteddate" => $posteddate
						, "wizard" => $wizard
						, "title" => $title
						, "article" => $article
					];
					$rows[] = $row;
				}		
			}

			// Tidy up
			$stmt->close();
			$connection->close();
		}
	}
	
	$json = json_encode($rows);
	header('Content-Type: application/json');
	header('Content-Length: '. strlen($json));

	print $json;
?>