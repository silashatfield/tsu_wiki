<?php
require_once('AuthPlugin.php');
 
class AuthTsunami extends AuthPlugin {
  
  //return whether $username is a valid username
  function userExists($username) {
    //since the username will be passed from our external source, this will probably always be true
    //however, the security paranoid says to check the data
    //you could do an LDAP verify here, just to be safe
 
    return true; //or return false if the username is invalid
  }
  
  //whether the given username and password authenticate
  function authenticate($username, $password) {
    //the external authentication actually handles this part, but we still need a security check
 
    //this form element will be set by our login script.  this security check is important!
    global $wgLoginFormKey;

	// grab XML output from auth interface
	$url = "http://tsunami:8002/auth?username=$username&password=$password";
	$xml = implode("", (@file($url)));
	if (!$xml) 
	{
		return false;
	}

	// setup XML parser

            // setup XML parser
            $this->in_auth = false;
            $this->current_element = "";
	$this->xml = array();
	$parser = xml_parser_create('');
	xml_set_object($parser, $this);
	xml_parser_set_option($parser, XML_OPTION_CASE_FOLDING, 0);
	xml_set_element_handler($parser, "element_start", "element_end");
	xml_set_character_data_handler($parser, "content");
	xml_parse($parser, $xml, true);

	if($this->xml["valid"] != "yes")
	{
		return false;
	}

  // Map any groups out
  // REMOVED THE BELOW LINES TO ALLOW ALL USERS THAT CAN AUTHENTICATE ACCESS TO THE WIKI - Sid 9/5/2019
	// if(!empty($this->xml["groups"])) 
	// {
	// 	$groups = explode(";", $this->xml["groups"]);
	// 	return in_array("wiki", $groups);
	// }

	return true;
  }
 
  //The authorization is external, so autocreate accounts as necessary
  function autoCreate() {
    return true;
  }
  
  //tell MediaWiki to not look in its database for user authentication and that our authentication method is all that counts
  function strict() {
    return true;
  }
 
  //this function gets called when the user is created
  //$user is an instance of the User class (see includes/User.php)
  function initUser(&$user, $autocreate = false) {
  }
 
  //if using MediaWiki 1.5, we have a new function to modify the UI template!
  function modifyUITemplate(&$template, &$type) {
    $template->set('useemail', false);
    $template->set('create', false);
    $template->set('domain', false);
    $template->set('usedomain', false);

  }
        /*
         *  element_start()
         *      Called by the XML parser when an element opens.
         */
        function element_start($parser, $element, $attrs = array()) {

            // store the current element name and initialise it if needed
            $this->current_element = $element;
            if ($this->in_auth) {
                $xml[$this->element] = "";
            }

            // check if we have entered the 'auth' element
            if (isset($this->element) && $this->element == "auth") {
                $this->in_auth = true;
            }

        }


        /*
         *  element_end()
         *      Called by the XML parser when an element closes.
         */
        function element_end($parser, $element, $attrs = array()) {

            // check if we have left the 'auth' element
            if (isset($this->element) && $this->element == "auth") {
                $this->in_auth = false;
            }

        }


        /*
         *  content()
         *      Called by the XML parser when character data is found.
         */
        function content($parser, $content) {

            // add the content to the current element unless it's whitespace
            if (strlen(trim($content))) {
                if(!isset($this->xml[$this->current_element]))
                    $this->xml[$this->current_element] = $content;
                else
                    $this->xml[$this->current_element] .= $content;
            }

        }

}
?>
