<?php
/*
 * Custom Permissions Scheme using Categories
 * based on Extension:NamespacePermissions by Petr Andreev
 *
 * Provides separate permissions for each action (read,edit,create,move) based
 * on category tags on pages.
 *
 * Author: Matthew Vernon
 * Additional Contributions by Carsten Zimmermann and Richard Hartmann
 *
 * Licensed under GPL v2 - use,change,enjoy
 *
 * Usage:
 *
 * require_once('extensions/CategoryPermissions.php');
 * $wgCategoryExclusive=array("Category:cat_name","Category:cat2_name");//deny acces to these categories for anyone not in the group
 * $wgGroupAlwaysAllow=''; //set a group name to ALWAYS allow access to this group
 * $wgGroupDefaultAllow=true; //set to true to allow everyone access to pages without a category
 *
 *
 * //add groups to category permissions by:
 * $wgGroupPermissions['group_name']['Category:categoryname_read']=true;
 * $wgGroupPermissions['group_name']['Category:categoryname_edit']=true;
 * $wgGroupPermissions['group_name']['Category:categoryname_move']=true;
 * $wgGroupPermissions['group_name']['Category:categoryname_create']=true;
 *
 * //allow access for a group to all categories like this
 * $wgGroupPermissions['group_name']['*_read']=true;
 */

// prevent this as entry point
if ( !defined( 'MEDIAWIKI' ) ) {
        die( 'This file is a MediaWiki extension and not a valid entry point' );
}

// register extension
$wgExtensionCredits['parserhook'][] = array(
	'path' => __FILE__,
	'name' => 'CategoryPermissions',
	'version' => '0.4',
	'author' => array( 'Matthew Vernon' ),
	'url' => 'https://www.mediawiki.org/wiki/Extension:CategoryPermissions',
	'description' => 'Provides user permission restrictions based on page categories',
);

//set up hook
$wgExtensionFunctions[] = "wfCategoryPermissions";

function wfCategoryPermissions()
{
  global $wgHooks;

  // use the userCan hook to check permissions
  $wgHooks[ 'userCan' ][] = 'checkCategoryPermissions';
}

//turn on debug messages by changing to 1 or true
// Remember to set $wgDebugLogFile in LocalSettings.php as well.
define("debug_permissions", 1);


function checkCategoryPermissions( $title, $user, $action, $result )
{
  global $wgGroupDefaultAllow, $wgGroupPermissions, $wgCategoryExclusive;

  $user_allowed=false;

  //get categories for this page
  $parentCategories=$title->getParentCategories();

  $is_sysops = false;
  $is_bot = false;
  $is_changesboard = false;
  foreach ($user->getGroups() as $value)
  {
    if($value == "sysop") $is_sysops = true;
    if($value == "bot") $is_bot = true;
  }

  if($is_sysops || $is_bot) return true;

  $result = true;
  if(is_array($parentCategories))
  {
    foreach( $parentCategories as $category=>$dd)
    {
      if($category == "Category:Changes_Board"){
        $is_changesboard = true;
      }
    }
  }

  if($is_changesboard && $action != "read"){
    MWDebug::init();
    MWDebug::log( json_encode($user->getGroups()) );
    MWDebug::log( "action: " . $action . " perm: DENIED" );
    return false;
  }

  return true;
}