<?php
/*
To enable this extension, put all files in this directory into a "help" subdirectory of your MediaWiki extensions directory.
Also, add 
	require_once ( "extensions/help/extension.php" ) ;
to your LocalSettings.php
The extension will then be accessed as [[Special:Help/<topic>]].
*/

if( !defined( 'MEDIAWIKI' ) ) die();

# Integrating into the MediaWiki environment

$wgExtensionCredits['help'][] = array(
        'name' => 'help',
        'description' => 'An extension to pull data from tsunami',
        'author' => 'Tsunami Team'
);

$wgExtensionFunctions[] = 'wfhelpExtension';

# for Special::Version:
$wgExtensionCredits['parserhook'][] = array(
        'name' => 'help extension',
        'author' => 'Tsunami Team',
        'url' => 'http://thebigwave.net',
        'version' => 'v0.01',
);

$wgSpecialPages['help'] = 'SpecialHelp';


#_____________________________________________________________________________

/**
 * The special page
 */
function wfhelpExtension() { # Checked for HTML and MySQL insertion attacks
	global $IP, $wgMessageCache;
	require_once $IP.'/includes/specialpage/SpecialPage.php';
	class SpecialHelp extends SpecialPage {
	
		/**
		* Constructor
		*/
		function __construct() {
			parent::__construct( 'help' );
		}

		/**
		* Special page main function
		*/
		function execute( $par = null ) { # Checked for HTML and MySQL insertion attacks
			global $wgOut, $wgRequest, $wgUser, $wgTitle, $IP;
			$request = $this->getRequest();
			$output = $this->getOutput();
			$this->setHeaders();

			$fname = 'Special::Tasks:execute';
			global $xmlg, $html_named_entities_mapping_mine, $content_provider;
			include_once ( "default.php" ) ; 
			$xmlg['sourcedir'] = $IP.'/extensions/help' ;
			include_once ( "help.php" ) ;
			
			$output->addHTML( $out );
		}
		
	} # end of class
}


?>
