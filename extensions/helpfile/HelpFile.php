<?php

if ( !defined( 'MEDIAWIKI' ) ) {
	die( 'This file is a MediaWiki extension, it is not a valid entry point' );
}

$wgExtensionFunctions[] = 'wfonParserFirstCallInit';
$wgExtensionCredits['parserhook'][] = array(
	'name' => 'ParserFunctions',
	'version' => '1.1.1',
	'url' => 'http://www.mediawiki.org/wiki/Extension:ParserFunctions',
	'author' => 'Tim Starling',
	'description' => 'Enhance parser with logical functions',
	'descriptionmsg' => 'pfunc_desc',
);

$wgExtensionMessagesFiles['ParserFunctions'] = dirname(__FILE__) . '/HelpFile.i18n.php';

// Register any render callbacks with the parser
function wfonParserFirstCallInit( ) {
   global $wgParser, $wgExtParserFunctions, $wgHooks;

   // Create a function hook associating the "example" magic word with renderExample()
   $wgParser->setFunctionHook( 'helpfile', 'renderExample' );
}

// Render the output of {{#example:}}.
function renderExample( Parser $parser, $param1 = '', $param2 = '', $param3 = '' ) {
	$output = file_get_contents("http://www.thebigwave.net/help.php?" . $param1);
	$output = preg_replace('/<a.*?>(.*?)<\/a>/', '<a href="/wiki/index.php/${1}">${1}</a>', $output);
	$output .= '
		<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>

		<script type="text/javascript">	
			$("pre").first().hide();
			$("table").first().find("td").first().hide();
			$("table").eq(1).hide();
			var $e = $("table").eq(1).parent().children("pre");
			$("table").eq(1).parent().empty().append($e);
		</script>
	';
	return [ $output, 'nowiki' => false,'noparse' => false, 'isHTML' => true ];
}

?>
