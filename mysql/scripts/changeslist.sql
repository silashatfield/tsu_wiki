select convert(replace(page_title,'Change_',''),UNSIGNED INTEGER) as ID
	,old_text
from tw_page
		inner join tw_revision on page_Latest = rev_id
		inner join tw_text on rev_text_id = old_id
where 1=1
	#MUST INCLUDE THESE FOR CHANGES BOARD
	and page_title like "Change_%"
    and old_text like '%Category:Changes_Board%'	

	#CASE ID = [num]
	and convert(replace(page_title,'Change_',''),UNSIGNED INTEGER) between 1000 and 1005

	#ORDER BY
#order by ID DESC

	#LIMIT
limit 50;