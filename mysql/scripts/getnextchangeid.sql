select convert(replace(page_title,'Change_',''),UNSIGNED INTEGER)+1 as ID
from tw_page
		inner join tw_revision on page_Latest = rev_id
		inner join tw_text on rev_text_id = old_id
where page_title like "Change_%"
    and old_text like '%Category:Changes_Board%'
order by ID DESC
limit 1;