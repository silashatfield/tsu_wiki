use wikidb;
DROP PROCEDURE IF EXISTS setautoincrement;
DELIMITER //
CREATE PROCEDURE setautoincrement()
BEGIN	
	DECLARE bdone INT;
	DECLARE var1 CHAR(50);
	DECLARE var2 CHAR(50);
	DECLARE curs CURSOR FOR select distinct table_name, column_name
		from information_schema.columns
		where table_schema = 'wikidb'
			  and table_name like 'tw_%'
			  and column_key = 'PRI'
			  and data_type = 'int'
			  and table_name not in (select table_name
									 from information_schema.columns
									 where table_schema = 'wikidb'
										   and column_key = 'PRI'
										   and data_type = 'int'
										   and extra = 'auto_increment');
                                   
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET bdone = 1;
  DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN END;
  OPEN curs;
  SET bdone = 0;
  REPEAT	
	FETCH curs INTO var1,var2;    
		SET @qry = CONCAT("ALTER TABLE `",var1,"` MODIFY `",var2,"` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT = 10000;");
		SELECT @qry;
		if(@qry is not null)
		then
			PREPARE stmt FROM @qry;
			EXECUTE stmt;
			DEALLOCATE PREPARE stmt;
		end if;	
	UNTIL bDone END REPEAT;
	CLOSE curs;
END;
//
DELIMITER;