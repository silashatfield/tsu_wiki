use wikidb;
DROP PROCEDURE IF EXISTS updatechangesboard;
DELIMITER //
CREATE PROCEDURE updatechangesboard()
BEGIN	
	update tw_page
		inner join tw_revision on page_Latest = rev_id
		inner join tw_text on rev_text_id = old_id
	set old_text = concat(old_text,'<br><br>[[Category:Changes_Board]]')
	where page_title like "Change_%"
		and old_text not like '%Category:Changes_Board%';
END;
//