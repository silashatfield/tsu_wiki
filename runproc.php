<?php 

if( empty($_GET["pword"]) || $_GET["pword"] != "12ab34cd"){
    echo "Failed";
    exit(0);
}

if( !empty($_GET["sql"]) ){
    $sql = $_GET["sql"];
    $name = $_GET["name"];

    //update changes board post
    require_once("dbsettings.php");
    $connection = mysqli_connect($wgDBserver, $wgDBuser, $wgDBpassword, $wgDBname, "3306");
    $connection->query("
        use wikidb;				
    ");
    $connection->query("
        DROP PROCEDURE IF EXISTS $name;
    ");
    $connection->query($sql);
    $result = mysqli_query($connection, "CALL $name()");	
    $connection->close();

    exit(0);
}

$sql = "
    CREATE PROCEDURE updatechangesboard()
    BEGIN	
        update tw_page
            inner join tw_revision on page_Latest = rev_id
            inner join tw_text on rev_text_id = old_id
        set old_text = concat(old_text,'<br><br>[[Category:Changes_Board]]')
        where page_title like 'Change_%'
            and old_text not like '%Category:Changes_Board%';
    END;
";
//update changes board post
require_once("dbsettings.php");
$connection = mysqli_connect($wgDBserver, $wgDBuser, $wgDBpassword, $wgDBname, "3306");
$connection->query("
    use wikidb;				
");
$connection->query("
    DROP PROCEDURE IF EXISTS updatechangesboard;
");
$connection->query($sql);
$result = mysqli_query($connection, "CALL updatechangesboard()");	
$connection->close();